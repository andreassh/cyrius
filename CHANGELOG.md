# Changelog

**v1.0.3**
Added *177, confirmed the ability to call newly added *4.035 as *4 and the modified *150.002 as *150 (PharmVar v6.1.7).

**v1.0.2**
Updated the phenotype reporting algorithm to handle more complex haplotype combinations than described in the CPIC genotype-phenotype prediction table. Added a feature to output haplotype-specific information (activity value, function, strength- and summary of evidence).

**v1.0.1**
Added *176 and confirmed that *1.065 is genotyped as *1 (PharmVar v6.1.5).

**v1.0.0**
Initial release of BCyrius, including star alleles up to *175 (based on PharmVar v6.1.3).
